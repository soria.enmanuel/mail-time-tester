'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('mail_queues', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      _id: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING
      },
      to: {
        type: Sequelize.STRING
      },
      tries: {
        type: Sequelize.INTEGER
      },
      sentAt: {
        type: Sequelize.DATE
      },
      isSent: {
        type: Sequelize.BOOLEAN
      },
      template: {
        type: Sequelize.STRING
      },
      transport: {
        type: Sequelize.INTEGER
      },
      concatSubject: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('mail_queues');
  }
};