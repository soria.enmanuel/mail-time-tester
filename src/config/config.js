require('dotenv').config()

module.exports = {
  siteUrl: process.env.SITE_HOST || 'http://localhost:4200',
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    dialect: process.env.DB_DIALECT
  }
}
