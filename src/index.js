'use strict'
require('dotenv').config()
const MailTime = require('mail-time');
const transports = [];
const nodemailer = require('nodemailer');
let { mailqueue } = require('./models')

// Google Apps SMTP
transports.push(nodemailer.createTransport({
  host: 'smtp.gmail.com',
  from: process.env.GMAIL_USER,
  auth: {
    user: process.env.GMAIL_USER,
    pass: process.env.GMAIL_PASS
  },
}));

transports.push(nodemailer.createTransport({
  service: 'Mailgun', // no need to set host or port etc.
  auth: {
      user: process.env.MAIL_DOMAIN,
      pass: process.env.MAILGUN_API
  }
}));

//init the SequelizeHandler with our model
let dbHandler = new MailTime.SequelizeHandler(mailqueue,  60 * 1000, 5);

const mailQueue = new MailTime({
  db: dbHandler, // Sequelize Class
  type: 'server',
  strategy: 'balancer', // Transports will be used in round robin chain
  transports,
  from(transport) {
    // To pass spam-filters `from` field should be correctly set
    // for each transport, check `transport` object for more options
    return '"Awesome App" <' + transport.options.from + '>';
  },
  concatEmails: true, // Concatenate emails to the same addressee
  concatDelimiter: '<h1>{{{subject}}}</h1>', // Start each concatenated email with it's own subject
  template: MailTime.Template // Use default template
});

// Schema:
    // _id
    // to          {String|[String]}
    // tries       {Number}  - qty of send attempts
    // sendAt      {Date}    - When letter should be sent
    // isSent      {Boolean} - Email status
    // template    {String}  - Template for this email
    // transport   {Number}  - Last used transport
    // concatSubject {String|Boolean} - Email concatenation subject
    // ---
    // mailOptions         {[Object]}  - Array of nodeMailer's `mailOptions`
    // mailOptions.to      {String|Array} - [REQUIRED]
    // mailOptions.from    {String}
    // mailOptions.text    {String|Boolean}
    // mailOptions.html    (String)
    // mailOptions.subject {String}
    // mailOptions.Other nodeMailer `sendMail` options...

