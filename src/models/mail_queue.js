'use strict';
module.exports = (sequelize, DataTypes) => {
  const mail_queue = sequelize.define('mailqueue', {
    _id: DataTypes.STRING,
    to: DataTypes.STRING,
    tries: DataTypes.INTEGER,
    sentAt: DataTypes.DATE,
    isSent: DataTypes.BOOLEAN,
    template: DataTypes.STRING,
    transport: DataTypes.INTEGER,
    concatSubject: DataTypes.STRING
  }, {tableName: 'mail_queue'});
  mail_queue.associate = function(models) {
    // associations can be defined here
  };
  return mail_queue;
};