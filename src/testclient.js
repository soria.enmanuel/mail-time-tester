'use strict'
require('dotenv').config()
const MailTime = require('mail-time');
let { mailqueue } = require('../models/index')

let dbHandler = new MailTime.SequelizeHandler(mailqueue,  60 * 1000, 5);

const mailQueue = new MailTime({
  dbHandler,
  type: 'client',
  strategy: 'balancer', // Transports will be used in round robin chain
  concatEmails: true // Concatenate emails to the same address
});

mailQueue.sendMail({
  to: 'soria.enmanuel@gmail.com',
  subject: 'You\'ve got an email!',
  text: 'Plain text message',
  html: '<h1>HTML</h1><p>Styled message</p>'
});
