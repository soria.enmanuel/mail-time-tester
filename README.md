# Mail-Time tester

this is a template to test [Mail-Time](https://github.com/edgedemon/Mail-Time) with sequelize

all we need to do is have a sequelize model init it with Mail-Time dataHandler for sequelize and give it in the options... in theory at least

## data structure

npx sequelize model:generate --name mail_queue --attributes _id:string,to:string,tries:integer,sentAt:date,isSent:boolean,template:string,transport:integer,concatSubject:string

## dependencies

we will be using sequelize with mysql, you can use your favourite RDBMS but you will need to install the proper instructions.

npm install --save nodemailer
npm install --save edgedemon/mail-time

npm install --save sequelize
npm install --save mysql2
npm install --save dotenv
